jQuery(document).ready(function ($) {

    if ($('.pretty-link-box-references').length) {
        $('.pretty-link-box.has-reference').each(function (i, obj) {
            $counter = (i + 1).toString();
            $refBox = $(this).children('.pretty-link-box-ref-box').eq(0);

            if ($refBox) {
                $(this).attr({'id': 'pretty-link-box-' + $counter});
                $refBox.css({'visibility': 'visible'});
                $refBox.html($counter);
            }
        });
    }
});