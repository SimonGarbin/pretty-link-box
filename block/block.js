(function (blocks, element, blockEditor, components, i18n) {

    var registerBlockType = blocks.registerBlockType;
    var el = element.createElement;
    var __ = i18n.__;

    var useBlockProps = blockEditor.useBlockProps;
    var InspectorControls = blockEditor.InspectorControls;
    var RichText = blockEditor.RichText;
    var LinkControl = blockEditor.__experimentalLinkControl;
    var MediaUpload = blockEditor.MediaUpload;
    var PanelColorSettings = blockEditor.PanelColorSettings;

    var PanelBody = components.PanelBody;
    var PanelRow = components.PanelRow;
    var BaseControl = components.BaseControl;
    var Button = components.Button;
    var TextControl = components.TextControl;
    var SelectControl = components.SelectControl;
    var ToggleControl = components.ToggleControl;
    var FontSizePicker = components.FontSizePicker;
    var NumberControl = components.__experimentalNumberControl;
    var UnitControl = components.__experimentalUnitControl;
    var BorderControl = components.__experimentalBorderControl;

    registerBlockType('pretty-link-box/block', {
        title: __('Pretty Link Box', 'pretty-link-box'),
        icon: 'format-image',
        category: 'widgets',
        supports: {
            anchor: true
        },
        attributes: {
            image: {
                type: 'object',
                default: {
                    id: null,
                    url: null,
                    slug: null,
                    dim: null,
                    sizes: null
                }
            },
            title: {
                type: 'string',
                default: null
            },
            titleTag: {
                type: 'string',
                default: 'h2'
            },
            subtitle: {
                type: 'string',
                default: null
            },
            subtitleTag: {
                type: 'string',
                default: 'span'
            },
            link: {
                type: 'object',
                default: {
                    url: null,
                    newTab: false
                }
            },
            reference: {
                type: 'string',
                default: null
            },
            anchor: {
                type: 'string',
                default: null
            },
            display: {
                type: 'string',
                default: null
            },
            width: {
                type: 'string',
                default: null
            },
            aspectRatio: {
                type: 'object',
                default: {
                    'x': null,
                    'y': null
                }
            },
            border: {
                type: 'object',
                default: null
            },
            borderRadius: {
                type: 'string',
                default: null
            },
            shadow: {
                type: 'boolean',
                default: false
            },
            titleFontSize: {
                type: 'string',
                default: null
            },
            fontColor: {
                type: 'string',
                default: null
            },
            bgColor: {
                type: 'string',
                default: null
            }
        },
        edit: function (props) {
            var attributes = props.attributes;
            var setAttributes = props.setAttributes;

            function onSelectImage(image) {
                var sizes = [];
                for (var slug in image.sizes) {
                    sizes.push({
                        slug: slug,
                        url: image.sizes[slug].url,
                        dim: image.sizes[slug].width + 'x' + image.sizes[slug].height
                    });
                }

                var dim = image.width + 'x' + image.height;
                var index = sizes.findIndex((obj => obj.dim === dim));
                var slug = null;
                if (index !== -1) {
                    slug = sizes[index].slug;
                }

                setAttributes({image: {
                        id: image.id,
                        url: image.url,
                        slug: slug,
                        dim: dim,
                        sizes: sizes
                    }});
            }

            function RemoveImage() {
                setAttributes({image: {
                        id: null,
                        url: null,
                        slug: null,
                        dim: null,
                        sizes: null
                    }});
            }

            function onChangeImageSize(newSlug) {
                var id = attributes.image.id;
                var sizes = attributes.image.sizes;

                var index = sizes.findIndex((obj => obj.slug === newSlug));
                if (index === -1) {
                    return;
                }

                var newUrl = sizes[index].url;
                var newDim = sizes[index].dim;

                setAttributes({image: {
                        id: id,
                        url: newUrl,
                        slug: newSlug,
                        dim: newDim,
                        sizes: sizes
                    }});
            }

            function onChangeTitle(newTitle) {
                setAttributes({title: newTitle || null});
            }

            function onChangeTitleTag(newTitleTag) {
                setAttributes({titleTag: newTitleTag});
            }

            function onChangeSubtitle(newSubtitle) {
                setAttributes({subtitle: newSubtitle || null});
            }

            function onChangeSubtitleTag(newSubitleTag) {
                setAttributes({subtitleTag: newSubitleTag});
            }

            function onChangeReference(newReference) {
                setAttributes({reference: newReference || null});
            }

            function onChangeLink(newLink, post) {
                setAttributes({link: {
                        url: newLink.url,
                        newTab: newLink.opensInNewTab
                    }});
            }

            function onRemoveLink() {
                setAttributes({link: {
                        url: null,
                        newTab: null
                    }});
            }

            function onChangeDisplay(newDisplay) {
                setAttributes({display: newDisplay});
            }

            function onChangeWidth(newWidth) {
                setAttributes({width: newWidth || null});
            }

            function onChangeAspectRatio(newXVal, newYVal) {
                setAttributes({
                    aspectRatio: {
                        x: newXVal,
                        y: newYVal
                    }
                });
            }

            function onChangeBorder(newBorder) {
                if (newBorder !== undefined) {
                    setAttributes({
                        border: {
                            color: newBorder.color,
                            style: newBorder.style,
                            width: newBorder.width
                        }
                    });
                } else {
                    setAttributes({border: null});
                }
            }

            function onChangeBorderRadius(newBorderRadius) {
                setAttributes({borderRadius: newBorderRadius || null});
            }

            function onChangeShadow(newShadow) {
                setAttributes({shadow: newShadow});
            }

            function onChangeTitleFontSize(newTitleFontSize) {
                setAttributes({titleFontSize: newTitleFontSize || null});
            }

            function onChangeFontColor(newFontColor) {
                setAttributes({fontColor: newFontColor || null});
            }

            function onChangeBgColor(newbgColor) {
                setAttributes({bgColor: newbgColor || null});
            }

            return el('div', useBlockProps(),
                    el('div', {
                        id: attributes.anchor,
                        className: 'pretty-link-box'
                                + (attributes.border ? ' has-border' : '')
                                + (attributes.shadow ? ' has-shadow' : '')
                                + (attributes.reference ? ' has-reference' : '')
                                + (attributes.display === 'animated' ? ' is-animated' : '')
                                + (attributes.display === 'bottom-title' ? ' has-bottom-title' : ''),
                        style: {
                            'width': attributes.width,
                            'aspect-ratio': attributes.aspectRatio['x'] + ' / ' + attributes.aspectRatio['y'],
                            'border-width': attributes.border ? attributes.border.width : null,
                            'border-style': attributes.border ? attributes.border.style : null,
                            'border-color': attributes.border ? attributes.border.color : null,
                            'border-radius': attributes.borderRadius
                        }
                    },
                            attributes.image.id && el('img', {
                                className: 'pretty-link-box-image',
                                src: attributes.image.url
                            }),
                            el('div', {
                                className: 'pretty-link-box-title-wrap',
                                style: {
                                    'color': attributes.fontColor
                                }
                            },
                                    el('div', {
                                        className: 'pretty-link-box-title-layer',
                                        style: {
                                            'background-color': attributes.bgColor
                                        }
                                    }),
                                    el('h2', {
                                        className: 'pretty-link-box-title',
                                        style: {
                                            'font-size': attributes.titleFontSize
                                        }
                                    },
                                            attributes.title
                                            ),
                                    el('span', {
                                        className: 'pretty-link-box-subtitle'
                                    },
                                            attributes.subtitle
                                            )
                                    ),
                            el('div', {
                                className: 'pretty-link-box-image-layer',
                                style: {
                                    'background-color': attributes.bgColor
                                }
                            }),
                            ),
                    el('div', {
                        className: 'pretty-link-box-ref-input-wrap'
                    },
                            el('p', {},
                                    el('b', {}, __('Image source', 'pretty-link-box'))),
                            el(RichText, {
                                tagName: 'p',
                                value: attributes.reference,
                                placeholder: __('...', 'pretty-link-box'),
                                onChange: onChangeReference
                            })
                            ),
                    el(InspectorControls, {group: 'settings'},
                            el(PanelBody, {title: __('General', 'pretty-link-box')},
                                    el(TextControl, {
                                        label: __('Title', 'pretty-link-box'),
                                        value: attributes.title,
                                        onChange: onChangeTitle
                                    }),
                                    el(TextControl, {
                                        label: __('Subtitle', 'pretty-link-box'),
                                        value: attributes.subtitle,
                                        onChange: onChangeSubtitle
                                    }),
                                    el(BaseControl, {
                                        id: 'pretty-link-box-media-button',
                                        label: __('Image', 'pretty-link-box')
                                    },
                                            el(MediaUpload, {

                                                onSelect: onSelectImage,
                                                allowedTypes: 'image',
                                                value: attributes.image.id,
                                                render: function (obj) {
                                                    return [
                                                        !attributes.image.id &&
                                                                el(Button, {
                                                                    id: 'pretty-link-box-media-button',
                                                                    onClick: obj.open,
                                                                    variant: 'secondary'
                                                                },
                                                                        __('Select', 'pretty-link-box')
                                                                        ),
                                                        attributes.image.id && [
                                                            el(Button, {
                                                                id: 'pretty-link-box-media-button',
                                                                onClick: obj.open,
                                                                variant: 'secondary'
                                                            },
                                                                    __('Replace', 'pretty-link-box')
                                                                    ),
                                                            el(Button, {
                                                                className: 'is-link is-destructive',
                                                                onClick: RemoveImage
                                                            },
                                                                    __('Remove', 'pretty-link-box')
                                                                    )
                                                        ]
                                                    ];
                                                }
                                            })
                                            ),
                                    attributes.image.sizes && el(PanelRow, {
                                        className: 'pretty-link-box-image-size-control'
                                    },
                                            el(SelectControl, {
                                                label: __('Image size', 'pretty-link-box'),
                                                labelPosition: 'side',
                                                className: 'pretty-link-box-select-control',
                                                value: attributes.image.slug,
                                                onChange: onChangeImageSize,
                                                help: attributes.image.dim,
                                                options: attributes.image.sizes.map(size => {
                                                    return {value: size.slug, label: size.slug};
                                                })
                                            })
                                            ),
                                    el(BaseControl, {
                                        label: __('Link', 'pretty-link-box'),
                                        className: 'pretty-link-box-link-control'
                                    },
                                            el(LinkControl, {
                                                value: {
                                                    url: attributes.link.url,
                                                    opensInNewTab: attributes.link.newTab
                                                },
                                                onChange: onChangeLink,
                                                onRemove: onRemoveLink
                                            })
                                            )
                                    ),
                            el(PanelBody, {
                                title: __('HTML tags', 'pretty-link-box')
                            },
                                    el(SelectControl, {
                                        label: __('Title', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-select-control',
                                        value: attributes.titleTag,
                                        onChange: onChangeTitleTag,
                                        options: [
                                            {value: 'span', label: 'span'},
                                            {value: 'p', label: 'p'},
                                            {value: 'h1', label: 'h1'},
                                            {value: 'h2', label: 'h2', selected: true},
                                            {value: 'h3', label: 'h3'},
                                            {value: 'h4', label: 'h4'}
                                        ]
                                    }),
                                    el(SelectControl, {
                                        label: __('Subtitle', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-select-control',
                                        value: attributes.subtitleTag,
                                        onChange: onChangeSubtitleTag,
                                        options: [
                                            {value: 'span', label: 'span', selected: true},
                                            {value: 'p', label: 'p'},
                                            {value: 'h1', label: 'h1'},
                                            {value: 'h2', label: 'h2'},
                                            {value: 'h3', label: 'h3'},
                                            {value: 'h4', label: 'h4'}
                                        ]
                                    })
                                    )
                            ),
                    el(InspectorControls, {group: 'styles'},
                            el(PanelBody, {
                                title: __('Size', 'pretty-link-box')
                            },
                                    el(UnitControl, {
                                        label: __('Width', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-input-control',
                                        value: attributes.width,
                                        onChange: onChangeWidth
                                    }),
                                    el(NumberControl, {
                                        label: __('Aspect ratio: width', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-input-control',
                                        value: attributes.aspectRatio['x'],
                                        onChange: function (val) {
                                            onChangeAspectRatio(val, attributes.aspectRatio['y']);
                                        },
                                        min: 1,
                                        max: 32,
                                        step: 1
                                    }),
                                    el(NumberControl, {
                                        label: __('Aspect ratio: height', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-input-control',
                                        value: attributes.aspectRatio['y'],
                                        onChange: function (val) {
                                            onChangeAspectRatio(attributes.aspectRatio['x'], val);
                                        },
                                        min: 1,
                                        max: 32,
                                        step: 1
                                    })
                                    ),
                            el(PanelBody, {
                                title: __('Style', 'pretty-link-box')
                            },
                                    el(SelectControl, {
                                        label: __('Display', 'pretty-link-box'),
                                        value: attributes.display,
                                        onChange: onChangeDisplay,
                                        options: [
                                            {
                                                value: 'animated',
                                                label: __('animated', 'pretty-link-box'),
                                                selected: true
                                            },
                                            {
                                                value: 'center-title',
                                                label: __('title always in center', 'pretty-link-box')
                                            },
                                            {
                                                value: 'bottom-title',
                                                label: __('title always at bottom', 'pretty-link-box')
                                            }

                                        ]
                                    }),
                                    el(ToggleControl, {
                                        checked: attributes.shadow,
                                        label: __('Shadow', 'pretty-link-box'),
                                        onChange: onChangeShadow
                                    }),
                                    el(BorderControl, {
                                        label: __('Border', 'pretty-link-box'),
                                        value: attributes.border,
                                        onChange: onChangeBorder,
                                        shouldSanitizeBorder: true
                                    }),
                                    el(UnitControl, {
                                        label: __('Corner radius', 'pretty-link-box'),
                                        labelPosition: 'side',
                                        className: 'pretty-link-box-input-control',
                                        value: attributes.borderRadius,
                                        onChange: onChangeBorderRadius
                                    })
                                    ),
                            el(PanelBody, {
                                title: __('Typography', 'pretty-link-box')
                            },
                                    el(FontSizePicker, {
                                        label: __('Size', 'pretty-link-box'),
                                        className: 'pretty-link-box-font-size-picker',
                                        value: attributes.titleFontSize,
                                        onChange: onChangeTitleFontSize,
                                        fontSizes: [
                                            {
                                                size: '1em',
                                                name: __('small', 'pretty-link-box'),
                                                slug: 'link-box-font-small'
                                            },
                                            {
                                                size: '1.6em',
                                                name: __('medium', 'pretty-link-box'),
                                                slug: 'link-box-font-medium'
                                            },
                                            {
                                                size: '2.2em',
                                                name: __('large', 'pretty-link-box'),
                                                slug: 'link-box-font-large'
                                            },
                                            {
                                                size: '2.8em',
                                                name: __('very large', 'pretty-link-box'),
                                                slug: 'link-box-font-very-large'
                                            }
                                        ],
                                        units: ['px', 'em', 'rem']
                                    })
                                    ),
                            el(PanelColorSettings, {
                                title: __('Colors', 'pretty-link-box'),
                                colorSettings: [
                                    {
                                        value: attributes.fontColor,
                                        onChange: onChangeFontColor,
                                        label: __('Text', 'pretty-link-box')
                                    },
                                    {
                                        value: attributes.bgColor,
                                        onChange: onChangeBgColor,
                                        label: __('Background', 'pretty-link-box')
                                    }
                                ]
                            })
                            ),
                    );
        },
        save: function () {
            return;
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.components,
        window.wp.i18n
        );