��    $      <  5   \      0     1     5     J  
   ^     i     p     w     �     �  	   �     �  
   �     �     �     �     �     �     �     �     �     �                           
   %     0     6     ?     E     L     R     i  
   �  �  �     e     i     �     �  	   �     �  	   �     �  	   �  	   �     �     �  
   �     �            	   !     +  
   4     ?     M     V     ^  
   d     o     t  
   z     �     �     �     �     �     �     �  
   �                                                  $                          	             !                               "                      #                    
                  ... Aspect ratio: height Aspect ratio: width Background Border Colors Corner radius Display General HTML tags Image Image size Image source Image sources Link Pretty Link Box Remove Replace Select Settings Shadow Size Style Subtitle Text Title Typography Width animated large medium small title always at bottom title always in center very large Project-Id-Version: Pretty Link Box
PO-Revision-Date: 2023-10-25 06:51+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: block/block.js
X-Poedit-SearchPath-1: pretty-link-box.php
 ... Seitenverhältnis: Höhe Seitenverhältnis: Breite Hintergrund Randlinie Farben Eckradius Darstellung Allgemein HTML-Tags Bild Bildgröße Bildquelle Bildquellen Link Hübsche Linkbox Entfernen Ersetzen Auswählen Einstellungen Schatten Größe Style Untertitel Text Titel Typografie Breite animiert groß mittel klein Titel immer unten Titel immer in der Mitte sehr groß 