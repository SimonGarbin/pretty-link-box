<?php

/**
 * Plugin Name: Pretty Link Box
 * Plugin URI: https://gitlab.com/SimonGarbin/pretty-link-box
 * Description: ...
 * Version: 1.0.0
 * Requires at least: 6.2
 * Requires PHP: 8.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: pretty-link-box
 * Domain Path: /languages

  Copyright 2023 Simon Garbin
  Pretty Link Box is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Pretty Link Box is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Pretty Link Box. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

function pretty_link_box_translation_dummy() {
    $plugin_description = __('...', 'pretty-link-box');
}

add_action(
        hook_name: 'init',
        callback: 'pretty_link_box_load_textdomain'
);

function pretty_link_box_load_textdomain() {
    load_plugin_textdomain(
            domain: 'pretty-link-box',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'pretty_link_box_settings_link'
);

function pretty_link_box_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=pretty_link_box&tab=general_options');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'pretty-link-box')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Link Boxes
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'pretty_link_box_register_block'
);

function pretty_link_box_register_block() {

    $asset_file = include(plugin_dir_path(__FILE__) . '/block/block.asset.php');

    $editor_style_handle = 'pretty-link-box-editor-style';
    $style_handle = 'pretty-link-box-style';
    $editor_script_handle = 'pretty-link-box-editor-script';
    $view_script_handle = 'pretty-link-box-view-script';

    wp_register_style(
            handle: $editor_style_handle,
            src: plugins_url('block/editor.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_style(
            handle: $style_handle,
            src: plugins_url('block/style.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_register_script(
            handle: $view_script_handle,
            src: plugins_url('assets/js/view-script.js', __FILE__),
            deps: array('jquery'),
            ver: false,
            in_footer: true
    );

    wp_register_script(
            handle: $editor_script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $editor_script_handle,
            domain: 'pretty-link-box',
            path: plugin_dir_path(__FILE__) . 'languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'pretty_link_box_render_callback',
            )
    );
}

function pretty_link_box_render_callback($attributes) {

    if (!isset($attributes['title']) || !$attributes['title']) {
        return '';
    }

    // Anchor and classes
    $anchor = '';

    if (isset($attributes['anchor']) && $attributes['anchor']) {
        $anchor .= sprintf(' id="%s"', esc_attr($attributes['anchor']));
    }

    $classes = array(
        'pretty-link-box',
    );

    if (isset($attributes['reference']) && $attributes['reference']) {
        $classes[] = 'has-reference';
    }

    if (isset($attributes['display']) && $attributes['display']) {
        if ($attributes['display'] == 'animated') {
            $classes[] = 'is-animated';
        } else if ($attributes['display'] == 'bottom-title') {
            $classes[] = 'has-bottom-title';
        } else if ($attributes['display'] == 'center-title') {
            $classes[] = 'has-center-title';
        }
    } else {
        $classes[] = 'is-animated';
    }

    if (isset($attributes['border']) && $attributes['border']) {
        $classes[] = 'has-border';
    }

    if (isset($attributes['shadow']) && $attributes['shadow']) {
        $classes[] = 'has-shadow';
    }

    if (isset($attributes['className']) && $attributes['className']) {
        $classes[] = $attributes['className'];
    }

    // Gather CSS properties
    $styles = array(
        'block' => array(),
        'layer' => array(),
        'image' => array(),
        'title-wrap' => array(),
        'title' => array(),
    );

    if (isset($attributes['width']) && $attributes['width']) {
        $styles['block'][] = 'width:' . esc_attr($attributes['width']);
    }

    if (isset($attributes['aspectRatio']) && $attributes['aspectRatio']) {
        if (!empty($attributes['aspectRatio']['x']) && !empty($attributes['aspectRatio']['y'])) {
            $styles['block'][] = sprintf(
                    'aspect-ratio:%s/%s',
                    esc_attr($attributes['aspectRatio']['x']),
                    esc_attr($attributes['aspectRatio']['y'])
            );
        }
    }

    if (isset($attributes['border']) && $attributes['border']) {
        foreach ($attributes['border'] as $key => $value) {
            $styles['block'][] = sprintf('border-%s:', $key) . esc_attr($value);
        }
    }

    if (isset($attributes['borderRadius']) && $attributes['borderRadius']) {
        $styles['block'][] = 'border-radius:' . esc_attr($attributes['borderRadius']);
    }

    if (isset($attributes['titleFontSize']) && $attributes['titleFontSize']) {
        $styles['title'][] = 'font-size:' . esc_attr($attributes['titleFontSize']);
    }

    if (isset($attributes['fontColor']) && $attributes['fontColor']) {
        $styles['title-wrap'][] = 'color:' . esc_attr($attributes['fontColor']);
    }

    if (isset($attributes['bgColor']) && $attributes['bgColor']) {
        $styles['layer'][] = 'background-color:' . esc_attr($attributes['bgColor']);
    }

    // Combine CSS properties to style attribute
    foreach ($styles as $key => $values) {
        if (!empty($values)) {
            $styles[$key] = sprintf(' style="%s;"', implode(';', $values));
        } else {
            $styles[$key] = '';
        }
    }

    // Generate HTML
    // --> Block
    $block = sprintf(
            '<div%1$s class="%2$s"%3$s>',
            $anchor,
            esc_attr(implode(' ', $classes)),
            $styles['block'],
    );

    // --> Image
    if (isset($attributes['image']) && $attributes['image']['id']) {
        $url = $attributes['image']['url'];
        $alt = get_post_meta(
                $attributes['image']['id'],
                '_wp_attachment_image_alt',
                TRUE
        );
        $block .= sprintf(
                '<img src="%s" alt="%s" class="pretty-link-box-image"%s>',
                esc_url($url),
                esc_attr($alt),
                $styles['image'],
        );
    }
    if (isset($attributes['reference']) && $attributes['reference']) {
        $block .= '<div class="pretty-link-box-ref-box" style="visibility: hidden;"></div>';
    }

    // --> Titles
    $block .= sprintf(
            '<div class="pretty-link-box-title-wrap"%s>',
            $styles['title-wrap'],
    );
    $block .= sprintf(
            '<div class="pretty-link-box-title-layer"%s></div>',
            $styles['layer'],
    );
    $block .= sprintf(
            '<%1$s class="pretty-link-box-title"%3$s>%2$s</%1$s>',
            esc_attr($attributes['titleTag']),
            esc_html($attributes['title']),
            $styles['title'],
    );
    if (isset($attributes['subtitle']) && $attributes['subtitle']) {
        $block .= sprintf(
                '<%1$s class="pretty-link-box-subtitle">%2$s</%1$s>',
                esc_attr($attributes['subtitleTag']),
                esc_html($attributes['subtitle']),
        );
    }
    $block .= '</div>';

    // --> Image layer
    // Note: Must be after title wrap for CSS rules to work
    $block .= sprintf(
            '<div class="pretty-link-box-image-layer"%s></div>',
            $styles['layer'],
    );

    // --> Link
    if (isset($attributes['link']) && isset($attributes['link']['url'])) {
        $new_tab = '';
        if (isset($attributes['link']['newTab']) && $attributes['link']['newTab']) {
            $new_tab = ' target="_blank"';
        }
        $block .= sprintf(
                '<a href="%s"%s class="pretty-link-box-link"></a>',
                esc_attr($attributes['link']['url']),
                $new_tab,
        );
    }

    $block .= '</div>';

    return $block;
}

add_shortcode(
        tag: 'PrettyLinkBoxRefs',
        callback: 'pretty_link_box_refs_shortcode'
);

function pretty_link_box_refs_shortcode($atts) {

    $allowed_html = array(
        'a' => array(
            'href' => array(),
            'target' => array()
        ),
        'b' => array(),
        'strong' => array(),
        'em' => array(),
        'i' => array(),
        'br' => array(),
        'sub' => array(),
        'sup' => array(),
        'mark' => array(),
        'del' => array()
    );

    $content = get_the_content();
    if (!$content) {
        return '';
    }

    $blocks = parse_blocks($content);
    if (!$blocks) {
        return '';
    }

    $references = pretty_link_box_parse_refs($blocks);
    if (!$references) {
        return '';
    }

    $ref_list = '<div class="pretty-link-box-references">';
    $ref_list .= '<p><b>' . __('Image sources', 'pretty-link-box') . '</b></p>';

    for ($i = 0; $i < count($references); $i++) {
        $ref_list .= sprintf(
                '<p><a href="#pretty-link-box-%1$s">%1$s</a> %2$s</p>',
                $i + 1,
                wp_kses(
                        content: $references[$i],
                        allowed_html: $allowed_html
                )
        );
    }

    $ref_list .= '</div>';

    return $ref_list;
}

function pretty_link_box_parse_refs($blocks, $refs = array()) {

    foreach ($blocks as $block) {
        if (isset($block['innerBlocks']) && !empty($block['innerBlocks'])) {
            $refs = pretty_link_box_parse_refs($block['innerBlocks'], $refs);
        } elseif ('pretty-link-box/block' === $block['blockName']) {
            if (isset($block['attrs']['reference']) && $block['attrs']['reference']) {
                $refs[] = $block['attrs']['reference'];
            }
        }
    }

    return $refs;
}
